package com.grupoica.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HolaServlet
 */
@WebServlet("/hola")
public class HolaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HolaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String strNombre = request.getParameter("nombre");

		String html = "<html><head><title>Formulario de envio</title></head><body>";
				if ( strNombre==null || "".equals(strNombre)) {
					html += "<h2> Pon el nombre </h2>";
				}else {
					html+="  <form action=\"./hola.do\" method=\"post\">\r\n" +
							" Veces:  <input name='veces' type=\"number\">\r\n" + 
							"  <input type=\"submit\" value=\"POST\">\r\n" + 
							"   </form> ";
				}
		html+="</body> </html>";
		response.getWriter().append(html);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		/*String strNumero = request.getParameter("veces");
		int numVeces = Integer.parseInt(strNumero);
		String html = "<html><head><title>Numero de veces</title></head><body>";

		for(int i=0;i<numVeces;i++) {
			html+=i+ "vez <br>";
		}
		html+="</body> </html>";

		response.getWriter().append(html);*/
		try(PrintWriter escritor = response.getWriter()){
			String strNumero = request.getParameter("veces");
			escritor.println("<html><head><title>Numero de veces</title></head><body>");
			if ( strNumero==null || "".equals(strNumero)) {
			System.out.println("Introduce un numero");
			escritor.println("<h2>Falta n� veces </h2>");
			}else {
				System.out.println("Usuario ha puesto " + strNumero + " veces");
				int numVeces = Integer.parseInt(strNumero);
				escritor.println("<ul>");
				for(int i=0;i<numVeces;i++) {
					escritor.println("<li> " + i + " vez </li>");
				}
				escritor.println("</ul>");
			}
			
			escritor.println("</body></html>");
			
		}
	}

}
