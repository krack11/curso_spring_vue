//JSON: Javacript Object Notation
//otra forma de crear objetos con la notacion JSON
let objetoVacio = {}; //new Object() es lo mismo

/*let formaPago = {
    "modo": "Tarjeta credito",
    "comision": 2,
    "activa": true,
    "preparacion": null,
    clientes: ["Santander", "Sabadell", "BBVA", [1, 23, 55]],
    "configuracion": {
        "conextion": "ssl",
        "latencia": 15
    }
};*/

let formaPago = JSON.parse(window.localStorage.getItem("datos-forma-pago"));
let arrayVacio = []; //new Array()
let datos = ["Churros", "Merinas", 200, true, null, { "ale": "mas datos" }];
let matriz = [
    [4, 6, 8],
    [3, 7, 7],
    [1, 5, 7]
];


formaPago.servidor = "http://visa.com";
formaPago["oculta"] = "Dame 5 centimos pa mi";
document.write(`<br>
<p>${formaPago.modo} - ${formaPago.clientes[1]} - ${formaPago.clientes[3][2]} </p>

${matriz[2][1]}
<h2>${JSON.stringify(formaPago)}</h2>
Usando forma HashMap: ${ formaPago["servidor"]}
`);

alert(JSON.stringify(formaPago, null, 3));
//Guardar en el disco el JSON en el espacio local de la página (unos 20Mb)
//Usando esto se podria guardar los datos para su uso sin conexion
window.localStorage.setItem("datos-forma-pago", JSON.stringify(formaPago, null, 3));

let petUsu = prompt("que dato quieres?");
document.write(`<br> ${formaPago[petUsu]}`);
let frutas = `[{"nombre": "pera", "precio": 20 },
            {"nombre": "kiwi", "precio": 27 },
            {"nombre": "fresa", "precio": 37 }]`;
let objFrutas = JSON.parse(frutas);
//Parse the data with JSON.parse(), and the data becomes a JavaScript object.
document.write(`<br> ${objFrutas[1].nombre } - ${objFrutas[2]["precio"] } `);