const PI = 3.1415926;

let unaVar = 20;
let unTexto = "Que pasa!";

document.write(`<br>
Texto en varias
lineas, y ademas podemos mostrar <br>
variables asi: ${unaVar} y otro texto: ${unTexto}`);
document.write("<br>");

//Funciones lamda: funciones anónimas o funciones flecha
var suma = (x, y) => x + y;
document.write("<br>" + suma(3, 2));
var alcuadrado = x => x ** 2;
document.write("<br>" + alcuadrado(3));

class Dato {
    constructor(x, y = 20) {
        this.x = x; //this.x es una variable miembro/ x es un parametro
        this.y = y;
    }
    mostrar() {
        document.write(`<br> Dato: x = ${this.x} y= ${this.y}`);
    }
}
class Info extends Dato {
    constructor(x, y = 20, z = 20) {
        super(x, y); //invocamos al constructor del padre nos referenciamos a this.x y this.y del constructor padre
        this.z = z;
    }
    mostrar() {
        super.mostrar();
        document.write(`z = ${this.z}`);
    }
}
let dato = new Dato("lo que quieras");
dato.mostrar();
let info = new Info("Otra info");
info.mostrar();