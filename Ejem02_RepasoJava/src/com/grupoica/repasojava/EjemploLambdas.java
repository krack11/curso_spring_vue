package com.grupoica.repasojava;

public class EjemploLambdas {
	
	//Una interfaz es funcional cuando solo tiene un m�todo
	interface NuestraFun {
		float operacion(float v, float w);
	}
	
	public static float sumar(float x, float y) {
		return x + y;
	}
	
	public static float restar(float x, float y) {
		return x - y;
	}
	
	public static float[] sumarArrays(float[] arr_1, float[] arr_2) {
		float[] arrRes = new float [arr_1.length];
		for(int i=0;i<arr_1.length && i < arr_2.length; i++) {
			arrRes[i] = arr_1[i] + arr_2[i];
		}
		return arrRes;
	}
	
	public static float[] restarArrays(float[] arr_1, float[] arr_2) {
		float[] arrRes = new float [arr_1.length];
		for(int i=0;i<arr_1.length && i < arr_2.length; i++) {
			arrRes[i] = arr_1[i] - arr_2[i];
		}
		return arrRes;
	}
	
	public static float[] operarArrays(float[] arr_1, float[] arr_2, NuestraFun funCallback ) {
		float[] arrRes = new float [arr_1.length];
		for(int i=0;i<arr_1.length && i < arr_2.length; i++) {
			arrRes[i] = funCallback.operacion(arr_1[i], arr_2[i]);
		}
		return arrRes;
	}
	
	
	public static void ejecutarLambdas() {
		float[] a = {25,54,45};
		float[] b = {5,6,7};
		float[] r= sumarArrays(a, b);
		for (float f : r) {
			System.out.print(" - " + f);
		}
		System.out.println("");
		float[] r2= operarArrays(a, b, EjemploLambdas::sumar);
		for (float f : r2) {
			System.out.print(" , " + f);
		}
		System.out.println("");
		float[] mult= operarArrays(a, b, (float x, float y) -> { System.out.println("Multiplicando " + x + " * " + y + " =" +  x * y);
			return x * y;
			});
		//Dos calculos sobre arrays:
		//resta usando funcion estatica y division usando lambda
		float[] r3 = restarArrays(a, b);
		
		for (float f : r3) {
			System.out.print(" , " + f);
		}
		System.out.println("");
		
		float[] r4= operarArrays(a, b, EjemploLambdas::restar);
		
		for (float f : r4) {
			System.out.print(" , " + f);

	}
		System.out.println("");
		float[] div= operarArrays(a, b, (float x, float y) -> { System.out.println("Dividiendo " + x + " / " + y + " =" +  x / y);
			return x / y;
		});

	
	}
}
	
	
