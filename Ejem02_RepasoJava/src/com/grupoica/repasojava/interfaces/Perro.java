package com.grupoica.repasojava.interfaces;

public class Perro implements Animal {
	
	String raza;

	public Perro(String raza) {
		this.raza = raza;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	@Override
	public void alimentarse(String comida) {
		
		System.out.println(getClass().getSimpleName() +" "+ raza + " Se alimenta de: " + comida);
		
	}
	
	
	
	

}
