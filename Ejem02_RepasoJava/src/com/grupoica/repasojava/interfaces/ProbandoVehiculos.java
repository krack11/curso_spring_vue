package com.grupoica.repasojava.interfaces;

import java.util.ArrayList;

public class ProbandoVehiculos {

	public ProbandoVehiculos() {
		// TODO Auto-generated constructor stub
	}
	
	public static void probar() {
		Coche miCoche = new Coche("Kia", 1500, 60.34f);
		miCoche.aceleracion();
		Coche miCocheFines = new Coche("Hammer", 3500, 85.34f);
		miCocheFines.aceleracion();
		Caballo miCaballo = new Caballo("Arabe", 55.23f, 40);
		miCaballo.aceleracion();
		//Polimorfismo: pero lo que se pasa es la referencia (el puntero, la direccion de memoria)
		Vehiculo unVehiculo = miCoche; //Casting implicito
		Object unObjeto = miCoche;
		Coche unCoche = (Coche) unObjeto; //Casting explicito
		System.out.println(unObjeto.toString());
		unVehiculo.aceleracion();

		Patinete unPatinete = new Patinete("nuevo");
		ArrayList<Motorizable> garaje = new ArrayList<>();
		garaje.add(miCoche);
		garaje.add(miCocheFines);
		garaje.add(unPatinete);
		
		//No se puede
		//garaje.add(new Vehiculo ("Que no he comprado",30));
		for(Motorizable motorizable : garaje) {
			motorizable.encender();
			if(motorizable instanceof Vehiculo) {
			((Coche) motorizable).aceleracion();
			((Coche) motorizable).desplazarse(1.5f);
			}
			
		}
		/*
		 * HashMap<String, Animal> granja = new HashMap<>();
		 * granja.put("perro",new Perro("Pastor Aleman"));
		 * granja.put("caballo",new Caballo(23));
		 * for(Map.Entry<String, Animal> animal : granja.entrySet(){
		 * animal.getValue().alimentarse("Pienso");
		 * 
		 * }
		 * 
		 * 
		 * 
		 * */
		
		Perro miPerro = new Perro("Pastor Aleman");
		ArrayList<Animal> granja = new ArrayList<>();
		granja.add(miCaballo);
		granja.add(miPerro);
		
		for(Animal animal : granja ) {
			animal.alimentarse("Pienso");
		}
		
		
		miCoche.encender();
		Motorizable vehMotor = miCoche;
		vehMotor.encender();
			
	}
	/*
	 * Ejercicios:
	 * 1- Garaje sera solo para objetos motorizables
	 * 2- Crear clase Patinete que sea motorizable pero no vehiculo
	 * 3- Guardaremos un patinete en el garaje
	 * 4- Hacer una clase Perro(que tampoco es un vehiculo)
	 * 5- Crear una interfaz Animal con metodo alimentarse(String comida)
	 * 6- Perro y Caballo que sean animales, y hacer una granja y alimentarlos.
	 */
	
	
	
	
	/*public static void probarCaballo() {
		Caballo miCaballo = new Caballo("Arabe", 55.23f, 40);
		miCaballo.aceleracion();
	}*/
	
	

}
