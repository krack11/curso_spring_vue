package com.grupoica.repasojava.interfaces;

public class Caballo extends Vehiculo implements Animal {
	int numDientes;

	public Caballo(String marca, float peso, int numDientes) {
		super(marca, peso);
		this.numDientes = numDientes;
		// TODO Auto-generated constructor stub
	}

	public int getNumDientes() {
		return numDientes;
	}

	public void setNumDientes(int numDientes) {
		this.numDientes = numDientes;
	}

	@Override
	public void aceleracion() {
		// TODO Auto-generated method stub
		//super.aceleracion();
		System.out.println(marca + " - Caballo trotando rapido!! ");
	}

	@Override
	public void desplazarse(float distancia) {
		System.out.println(marca + " trotando " + distancia + " metros");
		
	}

	@Override
	public void alimentarse(String comida) {
		System.out.println(getClass().getSimpleName() + " Se alimenta de: " + comida);
		
	}
	
	


	
	

}
