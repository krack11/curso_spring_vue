package com.grupoica.repasojava;

import java.util.HashMap;
import java.util.Scanner;

import com.grupoica.repasojava.modelo.Usuario;
//El hashmap es como un array o arraylist pero en vez de acceder a un elemento del array por el indice accedemos a traves de la clave que nosotros le pongamos
//en este caso la clave es String
public class EjemploHashMap {

	public static HashMap<String, Usuario> diccUsuarios  = new HashMap<>();
	
	public static void probandoHashMap() {
		diccUsuarios.put("Luis", new Usuario("Luis",18));
		diccUsuarios.put("Ana", new Usuario("Ana",20));
		diccUsuarios.put("Luisa", new Usuario("Luis",30));
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce el usuario:");
		String nombre = sc.nextLine();
		System.out.println("El usuario es " + diccUsuarios.get(nombre));
		
	}
	

}
