package com.grupoica.repasojava;

import java.util.ArrayList;

import com.grupoica.repasojava.interfaces.ProbandoVehiculos;
import com.grupoica.repasojava.modelo.GestionUsuarios;
import com.grupoica.repasojava.modelo.Loco;
import com.grupoica.repasojava.modelo.Usuario;

public class ProgramaMain {
	
	/*P.O.O.
	 * La unidad basica de almacenamiento son los tipos primitivos y los objetos
	 * que estan basados en clases. Las clases son el molde, plantilla o estructura que indica como seran todos los objetos instanciados
	 * a partir de ella: Sus variables miembre(campos, atributos, propiedades...) y sus metodos (funciones propias)
	 */
	
	public static void main(String[] args) {
		GestionUsuarios gesUsu = new GestionUsuarios();
		
		//ProbandoVehiculos.probar();
		EjemploLambdas.ejecutarLambdas();
		//gesUsu.aniadir("esto se aniade");
		//gesUsu.listarUsuarios();
		//EjemploMemoria.pruebaPasoPorValor();
		//EjemploMemoria.pruebaPorReferencia();
		
		//Ejercicios:
		//1 - Traer los ejemplos del contructor GestionUsuarios al Main.
		//this.listaUsuarios.add(10);
				/*Usuario usu = new Usuario();
				usu.setEdad(30);
				usu.setNombre("Fulanito Mengano");
				System.out.println("Nombre: " +  usu.getNombre());
				gesUsu.aniadir(usu);
				Usuario u2 = new Usuario("U2",50);
				gesUsu.aniadir(u2);
				System.out.println("Edad u2:" + u2.getEdad());
				//this.listaUsuarios.add("Texto");
				//this.listaUsuarios.add(new Object());
				gesUsu.listarUsuarios();
				
				if(u2.equals(usu)) {
					System.out.println("Son iguales");
				}else {
					System.out.println("Son distintos");
				}
				
				Loco joker = new Loco();
				joker.setNombre("Jokerino");
				joker.setTipoLocura(true);
				if(joker.isTipoLocura()) {
					System.out.println("Joker esta loco: " + joker.toString());
				}else {
					System.out.println("Joker no esta loco: " + joker.toString());
				}
				gesUsu.aniadir(joker);
				gesUsu.listarUsuario("Jokerino");
				gesUsu.aniadir("Fran", 14);
				gesUsu.listarUsuarios();*/
		//2-  Modificar Usuario(metodo para modificar nombre, edad y otro para los 2)
		/*gesUsu.modificarNombre("Juan","Funalito Mengano");
		gesUsu.modificarEdad(13);
		gesUsu.modificarEdadyNombre("Alberto", 40);
		gesUsu.listarUsuarios();
		 //3 - Eliminar un usuario
		//gesUsu.eliminarUsuario("Alberto");
		gesUsu.listarUsuarios();
		//3 - Eliminar Todos los usuarios
		//gesUsu.eliminarTodosUsuarios();
		gesUsu.listarUsuarios();
		//4 - Filtrar usuarios por edad (mostrar los que tengan cierta edad)
		gesUsu.listarPorEdad(40);*/
		//5 - Filtrar usuarios por edad (los que esten en un rango de edad)
		/*Usuario usu2 = new Usuario();
		usu2.setEdad(48);
		usu2.setNombre("Manolo");
		Usuario usu3 = new Usuario();
		usu3.setEdad(30);
		usu3.setNombre("Julio");
		gesUsu.aniadir(usu3);
		gesUsu.listarRangoEdad(10, 50);
		
		int ajedrez[][] = new int[8][8];
		ajedrez[1] = new int[3];*/
		/*EjemploHashMap ejemh = new EjemploHashMap();
		ejemh.probandoHashMap();*/
		 
		
	}
	
	
	
	

}
