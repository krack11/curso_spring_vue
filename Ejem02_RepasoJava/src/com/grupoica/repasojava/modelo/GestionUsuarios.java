package com.grupoica.repasojava.modelo;

import java.util.ArrayList;

import com.grupoica.repasojava.EjemploHashMap;

/*Clase que se encargará de las operaciones C.R.U.D
 * Create Read Update Delete (Op. Alta, Baja, modificacion y consulta)
 * 
 */
public class GestionUsuarios {
	// objeto en su forma ambigua (todos los elementos son object)
	// private ArrayList listaUsuarios; es lo mismo ArrayList<Object>
	// lista ees su forma genérica (todos los elementos son del mismo o de algun
	// tipo heredero)
	private ArrayList<Usuario> listaUsuarios;

	public GestionUsuarios() {
		super();
		this.listaUsuarios = new ArrayList();

	}

	public void listarUsuarios() {
		for (int i = 0; i < this.listaUsuarios.size(); i++) {
			System.out.println(this.listaUsuarios.get(i));
		}

	}

	public void listarUsuario(String nombre) {
		double doble = 10.3232;
		int entero = (int) doble; // Casting (coversion de un tipo de dato en otro)

		for (Usuario usu : listaUsuarios) {
			// if(usuObj instanceof Usuario) {
			// Usuario usu = (Usuario) usuObj; //Casting de objetos gracias al polimorfismo
			if (usu.getNombre().equals(nombre)) {
				System.out.println("ENCONTRADO: " + usu.getNombre());
			}
			// }
		}
	}

	public Usuario obtenerUsuario(String nombre) {

		for (Usuario usu : listaUsuarios) {
			if (usu.getNombre().equals(nombre)) {
				return usu;
			}
		}
		return null;
	}

	public void eliminarUsuario(String nombre) {
		Usuario usuBorrar = obtenerUsuario(nombre);
		listaUsuarios.remove(usuBorrar);
		/*for (Usuario usu : listaUsuarios) {
			if (usu.getNombre().equals(nombre)) {
				System.out.println("Usuario eliminado: " + nombre);
				listaUsuarios.remove(usu);
				return;
			}
		}*/
		System.out.println("Usuario no encontrado: " + nombre);

	}

	public void aniadir(Usuario usu) {
		this.listaUsuarios.add(usu);
		EjemploHashMap.diccUsuarios.put(usu.getNombre(), usu);

	}
	public void aniadir(String nombre, int edad) {
		Usuario nuevoUsu = new Usuario(nombre, edad);
		this.listaUsuarios.add(nuevoUsu);
		EjemploHashMap.diccUsuarios.put(nuevoUsu.getNombre(), nuevoUsu);
	}
	

	public void modificarNombre(String nombre, String nombreBuscar) {

		for (Usuario usu : this.listaUsuarios) {
			if(usu.getNombre().equals(nombreBuscar)) {
			usu.setNombre(nombre);
			}
		}
	}

	public void modificarEdad(int edad, String nombreBuscar) {

		for (Usuario usu : this.listaUsuarios) {
			if(usu.getNombre().equals(nombreBuscar)) {
			usu.setEdad(edad);
			}
		}

	}

	public void modificarEdadyNombre(String nombre, int edad, String nombreBuscar) {

		for (Usuario usu : this.listaUsuarios) {
			if(usu.getNombre().equals(nombreBuscar)) {
			usu.setNombre(nombre);
			usu.setEdad(edad);
			}
		}

	}

	public void eliminarTodosUsuarios() {
		this.listaUsuarios.clear();

	}

	/*public void listarPorEdad(int edad) {
		for (Usuario usu : listaUsuarios) {
			if (usu.getEdad() == (edad)) {
				System.out.println(listaUsuarios);
			}else {
				System.out.println("Usuario no encontrado");
				break;
			}

		}
	}

	public void listarRangoEdad(int min, int max) {
		
		for (Usuario usu : listaUsuarios) {
			if (usu.getEdad() >= min && usu.getEdad() <= max) {
				System.out.println(listaUsuarios);
			}else {
				System.out.println("Usuario no encontrado");
				break;
			}
			
		}
	}*/
	public void filtrarUsuario(int edadMin, int edadMax) {

		boolean encontrado = false;			
		
		for (Usuario usu : listaUsuarios) 
			if (usu.getEdad() >= edadMin && usu.getEdad() <= edadMax) {
				encontrado = true;
				System.out.println("Usuario encontrado: " + usu.toString());
			}
		
		if (! encontrado)
			System.out.println("Usuarios por edad no encontrados: ");
	}
	
	
	public void filtrarUsuario(int edad) {
		filtrarUsuario(edad, edad);
	}
	
	
	

}
