package com.grupoica.repasojava.modelo;
/*POJO*/
public class Usuario {
	
	private int edad;
	private String nombre;
	
	public Usuario() {
		super();
		nombre="Sin nombre";
	}
	
	public Usuario(String nombre, int edad) {
		super();
		this.nombre = nombre;
		this.edad = edad;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/*@Override
    public boolean equals (Object obj) {

        if (obj instanceof Usuario) {

        Usuario usuario= (Usuario) obj;

            if (this.nombre.equals(usuario.nombre)  && this.edad == usuario.edad) { 
            	
            	return true; 
            	
            } else {
            	
            	return false; 
            }

        } else { 
        	
        	return false; 
        }

	} */
	@Override
	public boolean equals(Object obj) {
		Usuario usuario = (Usuario)obj;
		return this.nombre == usuario.nombre && this.edad == usuario.edad;
	}
	
	public boolean equals(Usuario usuario) {
		return this.nombre == usuario.nombre && this.edad == usuario.edad;
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Usuario " + nombre + " [ " + edad + " ] ";
	}
	
	
	
	

}
