package com.grupoica.repasojava.modelo;

public class Loco extends Usuario {
	
	private boolean tipoLocura;
	
	public Loco() {
		super();
	}

	public boolean isTipoLocura() {
		return tipoLocura;
	}


	public void setTipoLocura(boolean tipoLocura) {
		this.tipoLocura = tipoLocura;
	}
	
	@Override
	public boolean equals(Object obj) {
		Loco loco = (Loco)obj;
		return this.tipoLocura == loco.tipoLocura;
	}
	
	public boolean equals(Loco loco) {
		return this.tipoLocura == loco.tipoLocura;
	}
	


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Nombre: " + getNombre()+ " Edad: " + getEdad() + " tipoLocura: " + tipoLocura;
	}
	
	

}
